## NotifyMe

The purpose of this project is to get in practice with local and push notifications in React Native.

Following the expo guide you should perform first a [EAS build](https://docs.expo.dev/build/introduction/) of this application on your own Expo dashboard, in order to have a real projectId that can be used to acquire and send notifications.

Try to push a notification from this [page](https://expo.dev/notifications)!

This project is a fun playground exercise taken from this RN great [course](https://github.com/academind/react-native-practical-guide-code/tree/15-notifications)
